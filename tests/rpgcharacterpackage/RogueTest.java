package rpgcharacterpackage;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RogueTest {
    @Test
    public void levelUp_rogueLevelShouldOnlyGetOneLevel_ExpectReturnIntOfOne(){
        Rogue rogue = new Rogue();
        int expected = 2;
        int actual = rogue.newLevel();
        assertEquals(expected, actual);
    }
    @Test
    public void levelOfNewChar_rogueLevelShouldOnlyBeLevelOneWhenCreated_ExpectedReturnIntOfOne(){
        Rogue rogue = new Rogue();
        int expected = 1;
        int actual = rogue.getLevel();
        assertEquals(expected, actual);
    }

    @Test
    public void startupAttributesForRogue_rogueShouldStartWithAPresetOfAttributesBeforeAnyEquipmentOrLevelingHasHappened_ExpectedReturnStringOfCorrectAttributes(){
        Rogue rogue = new Rogue();
        String expected = "1, 6, 2";
        String test = rogue.getIntAttribute() + ", " + rogue.getDexAttribute() + ", " + rogue.getStrAttribute();
        assertEquals(expected, test);
    }

    @Test
    public void attributesAfterARogueHasLeveledUp_rogueShouldGetAPresetOfAttributesAddedToTheirAttributesWhenLevelingUp_ExpectedReturnStringOfCorrectAttributesAfterMageHasGainedALevel(){
        Rogue rogue = new Rogue();
        String expected = "Intelligence = 2, Dexterity = 10, Strength = 3";
        String actual = "Intelligence = " + (rogue.getIntAttribute() + rogue.levelUpInt()) + ", Dexterity = " + (rogue.getDexAttribute() + rogue.levelUpDex()) + ", Strength = " + (rogue.getStrAttribute() + rogue.levelUpStr());
        assertEquals(expected, actual);
    }
}