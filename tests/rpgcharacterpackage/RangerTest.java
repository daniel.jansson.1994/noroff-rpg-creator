package rpgcharacterpackage;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangerTest {
    @Test
    public void levelUp_rangerLevelShouldOnlyGetOneLevel_ExpectReturnIntOfOne(){
        Ranger ranger = new Ranger();
        int expected = 2;
        int actual = ranger.newLevel();
        assertEquals(expected, actual);
    }
    @Test
    public void levelOfNewChar_mageLevelShouldOnlyBeLevelOneWhenCreated_ExpectedReturnIntOfOne(){
        Ranger ranger = new Ranger();
        int expected = 1;
        int actual = ranger.getLevel();
        assertEquals(expected, actual);
    }

    @Test
    public void startupAttributesForMage_mageShouldStartWithAPresetOfAttributesBeforeAnyEquipmentOrLevelingHasHappened_ExpectedReturnStringOfCorrectAttributes(){
        Ranger ranger = new Ranger();
        String expected = "1, 7, 1";
        String test = ranger.getIntAttribute() + ", " + ranger.getDexAttribute() + ", " + ranger.getStrAttribute();
        assertEquals(expected, test);
    }

    @Test
    public void attributesAfterAMageHasLeveledUp_mageShouldGetAPresetOfAttributesAddedToTheirAttributesWhenLevelingUp_ExpectedReturnStringOfCorrectAttributesAfterMageHasGainedALevel(){
        Ranger ranger = new Ranger();
        String expected = "Intelligence = 2, Dexterity = 12, Strength = 2";
        String actual = "Intelligence = " + (ranger.getIntAttribute() + ranger.levelUpInt()) + ", Dexterity = " + (ranger.getDexAttribute() + ranger.levelUpDex()) + ", Strength = " + (ranger.getStrAttribute() + ranger.levelUpStr());
        assertEquals(expected, actual);
    }
}