package rpgcharacterpackage;

import org.junit.jupiter.api.Test;
import rpgequipmentpackage.Armor;
import rpgequipmentpackage.Weapons;
import rpggame.Game;

import static org.junit.jupiter.api.Assertions.*;

public class EquipmentTest {
    private String warriorWeaponType;

    @Test
    public void calculateDps_calculatesTheDpsOfALevelOneCharacter_expectDoubleDpsReturn() {
        Game game = new Game();
        Warrior warrior = new Warrior();
        double test = game.calculateDps(0, 0, warrior.getStrAttribute());
        double expected = 1.05;

        assertEquals(expected, test);
    }

    @Test
    public void calculateDps_WithWeapon_test() {
        Game game = new Game();
        Warrior warrior = new Warrior();
        double test = game.calculateDps(7, 1.1, warrior.getStrAttribute());
        double expected = 8.085;

        assertEquals(expected, test);
    }

    @Test
    public void dps_with_armorandweap() {
        Game game = new Game();
        Warrior warrior = new Warrior();
        Armor armor = new Armor("Test", "plate", "helmet", 1, 5, 1, 2);
        double test = game.calculateDps(7, 1.1, (warrior.getStrAttribute() + armor.getStrGain()));
        double expected = 8.470000000000002;

        assertEquals(expected, test);
    }

    @Test
    public void gear_test_equipment() {
        String warriorItemType = "";
        Game game = new Game();
        Warrior warrior = new Warrior();
        Armor armor = new Armor("Test", "PLATE", "helmet", 1, 5, 1, 2);
        for (int i = 0; i < warrior.getGearType().length; i++) {
            if (warrior.getGearType()[i].equals(armor.getItemType())) {
                warriorItemType = warrior.getGearType()[i];
            }
        }
        boolean test = game.equipMethod(armor.getItemType(), warriorItemType, armor.getLevelRequirement(), warrior.getLevel());
        assertTrue(test);
    }

    @Test
    public void weapon_test_equipment() {
        Game game = new Game();
        Warrior warrior = new Warrior();
        Weapons weapon = new Weapons();
        weapon.setWeaponType("AXE");
        weapon.setLevelRequirement(1);
        String warriorWeaponType = "";

        for (int i = 0; i < warrior.getUsableWeapons().length; i++) {
            if (warrior.getUsableWeapons()[i].equals(weapon.getWeaponType())) {
                warriorWeaponType = warrior.getUsableWeapons()[i];
            }
        }
        boolean actual = game.equipMethod(weapon.getWeaponType(), warriorWeaponType, weapon.getLevelRequirement(), warrior.getLevel());

        assertTrue(actual);
    }

    @Test
    public void levelofWeapon_fix_testname() {
        Game game = new Game();
        Warrior warrior = new Warrior();
        Weapons weapon = new Weapons();
        weapon.setWeaponType("AXE");
        weapon.setLevelRequirement(4);
        String warriorWeaponType = "";

        for (int i = 0; i < warrior.getUsableWeapons().length; i++) {
            if (warrior.getUsableWeapons()[i].equals(weapon.getWeaponType())) {
                warriorWeaponType = warrior.getUsableWeapons()[i];
            }
        }

        boolean actual = game.equipMethod(weapon.getWeaponType(), warriorWeaponType, weapon.getLevelRequirement(), warrior.getLevel());
        assertFalse(actual);
    }

    @Test
    public void gfs_gfds_Gfds() {
        Game game = new Game();
        Warrior warrior = new Warrior();
        Weapons weapon = new Weapons();
        weapon.setWeaponType("STAFF");
        weapon.setLevelRequirement(1);
        String warriorWeaponType = "";

        for (int i = 0; i < warrior.getUsableWeapons().length; i++) {
            if (warrior.getUsableWeapons()[i].equals(weapon.getWeaponType())) {
                warriorWeaponType = warrior.getUsableWeapons()[i];
            }
        }

        boolean actual = game.equipMethod(weapon.getWeaponType(), warriorWeaponType, weapon.getLevelRequirement(), warrior.getLevel());
        assertFalse(actual);
    }

    @Test
    public void armortypefixitemType_name_later() {
        Game game = new Game();
        Warrior warrior = new Warrior();
        Armor armor = new Armor("Rabadon", "cloth", "helmet", 1, 4, 3, 2);
        String warriorWeaponType = "";

        for (int i = 0; i < warrior.getUsableWeapons().length; i++) {
            if (warrior.getUsableWeapons()[i].equals(armor.getItemType())) {
                warriorWeaponType = warrior.getUsableWeapons()[i];
            }
        }

        boolean actual = game.equipMethod(armor.getItemType(), warriorWeaponType, armor.getLevelRequirement(), warrior.getLevel());
        assertFalse(actual);
    }

    @Test
    public void armortypefixlevel_name_later() {
        Game game = new Game();
        Warrior warrior = new Warrior();
        Armor armor = new Armor("Rabadon", "cloth", "helmet", 1, 4, 3, 2);
        String warriorWeaponType = "";

        for (int i = 0; i < warrior.getUsableWeapons().length; i++) {
            if (warrior.getUsableWeapons()[i].equals(armor.getItemType())) {
                warriorWeaponType = warrior.getUsableWeapons()[i];
            }
        }

        boolean actual = game.equipMethod(armor.getItemType(), warriorWeaponType, armor.getLevelRequirement(), warrior.getLevel());
        assertFalse(actual);
    }
}
