package rpgcharacterpackage;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {
    @Test
    public void levelUp_warriorLevelShouldOnlyGetOneLevel_ExpectReturnIntOfOne(){
        Warrior warrior = new Warrior();
        int expected = 2;
        int actual = warrior.newLevel();
        assertEquals(expected, actual);
    }
    @Test
    public void levelOfNewChar_warriorLevelShouldOnlyBeLevelOneWhenCreated_ExpectedReturnIntOfOne(){
        Warrior warrior = new Warrior();
        int expected = 1;
        int actual = warrior.getLevel();
        assertEquals(expected, actual);
    }

    @Test
    public void startupAttributesForWarrior_warriorShouldStartWithAPresetOfAttributesBeforeAnyEquipmentOrLevelingHasHappened_ExpectedReturnStringOfCorrectAttributes(){
        Warrior warrior = new Warrior();
        String expected = "1, 2, 5";
        String test = warrior.getIntAttribute() + ", " + warrior.getDexAttribute() + ", " + warrior.getStrAttribute();
        assertEquals(expected, test);
    }

    @Test
    public void attributesAfterAWarriorHasLeveledUp_warriorShouldGetAPresetOfAttributesAddedToTheirAttributesWhenLevelingUp_ExpectedReturnStringOfCorrectAttributesAfterMageHasGainedALevel(){
        Warrior warrior = new Warrior();
        String expected = "Intelligence = 2, Dexterity = 4, Strength = 8";
        String actual = "Intelligence = " + (warrior.getIntAttribute() + warrior.levelUpInt()) + ", Dexterity = " + (warrior.getDexAttribute() + warrior.levelUpDex()) + ", Strength = " + (warrior.getStrAttribute() + warrior.levelUpStr());
        assertEquals(expected, actual);
    }
}