package rpgcharacterpackage;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {
    @Test
    public void levelUp_mageLevelShouldOnlyGetOneLevel_ExpectReturnIntOfOne(){
        Mage mage = new Mage();
        int expected = 2;
        int actual = mage.newLevel();
        assertEquals(expected, actual);
    }
    @Test
    public void levelOfNewChar_mageLevelShouldOnlyBeLevelOneWhenCreated_ExpectedReturnIntOfOne(){
        Mage mage = new Mage();
        int expected = 1;
        int actual = mage.getLevel();
        assertEquals(expected, actual);
    }

    @Test
    public void startupAttributesForMage_mageShouldStartWithAPresetOfAttributesBeforeAnyEquipmentOrLevelingHasHappened_ExpectedReturnStringOfCorrectAttributes(){
        Mage mage = new Mage();
        String expected = "8, 1, 1";
        String test = mage.getIntAttribute() + ", " + mage.getDexAttribute() + ", " + mage.getStrAttribute();
        assertEquals(expected, test);
    }

    @Test
    public void attributesAfterAMageHasLeveledUp_mageShouldGetAPresetOfAttributesAddedToTheirAttributesWhenLevelingUp_ExpectedReturnStringOfCorrectAttributesAfterMageHasGainedALevel(){
        Mage mage = new Mage();
        String expected = "Intelligence = 13, Dexterity = 2, Strength = 2";
        String actual = "Intelligence = " + (mage.getIntAttribute() + mage.levelUpInt()) + ", Dexterity = " + (mage.getDexAttribute() + mage.levelUpDex()) + ", Strength = " + (mage.getStrAttribute() + mage.levelUpStr());
        assertEquals(expected, actual);
    }
}