package rpggame;

import exceptionspackage.InvalidArmorException;
import exceptionspackage.InvalidWeaponException;
import rpgcharacterpackage.*;
import rpgequipmentpackage.Armor;
import rpgequipmentpackage.Weapons;

import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

public class Game {

    PlayerCharacter player = new PlayerCharacter() {
    };
    Mage mage = new Mage();
    Weapons weapon = new Weapons();
    Warrior warrior = new Warrior();
    Rogue rogue = new Rogue();
    Ranger ranger = new Ranger();

    public void characterSelector() {
        String[] classes = {"MAGE", "ROGUE", "WARRIOR", "RANGER"};
        String name, playerClass;
        Scanner answer = new Scanner(System.in);
        boolean classpicker = true;
        System.out.println("Enter your name: ");
        name = answer.nextLine();

        do {
            System.out.println("Enter the class of your choice: ");
            System.out.println("(mage, warrior, rogue, ranger).");
            playerClass = answer.nextLine();
            for (String aClass : classes) {
                if (playerClass.toUpperCase(Locale.ROOT).equals(aClass)) {
                    player.setPlayerClass(playerClass.toLowerCase(Locale.ROOT));
                    player.setName(name);
                    classpicker = false;
                }
            }
        } while (classpicker);
    }


    public void levelup() {
        if (player.getPlayerClass().equals("mage")) {
            mage.levelUp();
            System.out.println("Your level got increased by 1, new level: " + mage.getLevel());
        }
        if (player.getPlayerClass().equals("warrior")) {
            warrior.levelUp();
            System.out.println("Your level got increased by 1, new level: " + warrior.getLevel());
        }
        if (player.getPlayerClass().equals("rogue")) {
            rogue.levelUp();
            System.out.println("Your level got increased by 1, new level: " + rogue.getLevel());
        }
        if (player.getPlayerClass().equals("ranger")) {
            ranger.levelUp();
            System.out.println("Your level got increased by 1, new level: " + ranger.getLevel());
        }
    }

    public void stats() {
        //In setTotalAttributes, add in stats for all gear pieces.
        gearChecker();
        if (player.getPlayerClass().equals("mage")) {

            mage.setTotalAttributes((player.getHelmetStrength() + player.getChestStrength() + player.getLeggingsStrength()), (player.getHelmetIntelligence() + player.getChestIntelligence() + player.getLeggingsIntelligence()), (player.getHelmetDexterity() + player.getChestDexterity() + player.getLeggingsDexterity()));
            mage.setCharDps(calculateDps(weapon.getDamage(), weapon.getAttackSpeed(), (mage.getTotalAttributes().get("Intelligence") + mage.getBasicAttributes().get("Intelligence"))));

            System.out.print(mage.toString());
        }

        if (player.getPlayerClass().equals("warrior")) {
            warrior.setTotalAttributes((player.getHelmetStrength() + player.getChestStrength() + player.getLeggingsStrength()), (player.getHelmetIntelligence() + player.getChestIntelligence() + player.getLeggingsIntelligence()), (player.getHelmetDexterity() + player.getChestDexterity() + player.getLeggingsDexterity()));
            warrior.setCharDps(calculateDps(weapon.getDamage(), weapon.getAttackSpeed(), (warrior.getTotalAttributes().get("Strength") + warrior.getBasicAttributes().get("Strength"))));
            System.out.print(warrior.toString());
        }

        if (player.getPlayerClass().equals("rogue")) {
            rogue.setTotalAttributes((player.getHelmetStrength() + player.getChestStrength() + player.getLeggingsStrength()), (player.getHelmetIntelligence() + player.getChestIntelligence() + player.getLeggingsIntelligence()), (player.getHelmetDexterity() + player.getChestDexterity() + player.getLeggingsDexterity()));
            rogue.setCharDps(calculateDps(weapon.getDamage(), weapon.getAttackSpeed(), (rogue.getTotalAttributes().get("Dexterity") + rogue.getBasicAttributes().get("Dexterity"))));
            System.out.print(rogue.toString());
        }

        if (player.getPlayerClass().equals("ranger")) {
            ranger.setTotalAttributes((player.getHelmetStrength() + player.getChestStrength() + player.getLeggingsStrength()), (player.getHelmetIntelligence() + player.getChestIntelligence() + player.getLeggingsIntelligence()), (player.getHelmetDexterity() + player.getChestDexterity() + player.getLeggingsDexterity()));
            ranger.setCharDps(calculateDps(weapon.getDamage(), weapon.getAttackSpeed(), (ranger.getTotalAttributes().get("Dexterity") + ranger.getBasicAttributes().get("Dexterity"))));
            System.out.print(ranger.toString());
        }


    }

    public void selectGear() {
        boolean gearing = true;
        do {
            System.out.print("Select the gear piece you want to make or exit.\n");
            System.out.print("Options: helmet, chest, leggings, weapon, end.\n");

            String option;
            Scanner selected = new Scanner(System.in);
            option = selected.nextLine();

            switch (option) {
                case "helmet" -> {
                    makeArmor(option);
                    gearing = false;
                }
                case "chest" -> makeArmor(option);
                case "leggings" -> makeArmor(option);
                case "weapon" -> makeWeapon();
                case "end" -> gearing = false;
                default -> System.out.print("Please select an existing value!\n");
            }
        }
        while (gearing);
    }

    public void makeArmor(String gear) {
        Random randomNumber = new Random();
        String armorname;
        String itemType;
        String gearType = "";
        int intAttribute = 1 + randomNumber.nextInt(100);
        int strAttribute = 1 + randomNumber.nextInt(100);
        int dexAttribute = 1 + randomNumber.nextInt(100);
        //int itemLevel = 1 + randomNumber.nextInt(25);
        Scanner answer = new Scanner(System.in);
        System.out.print("Name your armor: ");
        armorname = answer.nextLine();
        System.out.println("What type of gear is this? ");
        itemType = answer.nextLine().toLowerCase(Locale.ROOT);

        Armor equipment = new Armor(armorname, itemType, gear, 1, strAttribute, dexAttribute, intAttribute);

        try {

            if (player.getPlayerClass().equals("mage")) {
                if (!equipMethod(equipment.getItemType(), mage.getGearType(), equipment.getLevelRequirement(), player.getLevel())) {
                    throw new InvalidArmorException("test");
                }
            }

            if (player.getPlayerClass().equals("warrior")) {
                for (int i = 0; i < warrior.getGearType().length; i++) {
                    if (itemType.toUpperCase(Locale.ROOT).equals(warrior.getGearType()[i])) {
                        gearType = warrior.getGearType()[i];
                    }
                }
                if (!equipMethod(equipment.getItemType().toUpperCase(Locale.ROOT), gearType.toUpperCase(Locale.ROOT), equipment.getLevelRequirement(), player.getLevel())) {
                    throw new InvalidArmorException("test");
                }
            }

            if (player.getPlayerClass().equals("rogue")) {
                for (int i = 0; i < rogue.getGearType().length; i++) {
                    if (itemType.toUpperCase(Locale.ROOT).equals(rogue.getGearType()[i])) {
                        gearType = rogue.getGearType()[i];
                    }
                }
                if (!equipMethod(equipment.getItemType().toUpperCase(Locale.ROOT), gearType.toUpperCase(Locale.ROOT), equipment.getLevelRequirement(), player.getLevel())) {
                    throw new InvalidArmorException("test");
                }
            }

            if (player.getPlayerClass().equals("ranger")) {
                for (int i = 0; i < ranger.getGearType().length; i++) {
                    if (itemType.toUpperCase(Locale.ROOT).equals(ranger.getGearType()[i])) {
                        gearType = ranger.getGearType()[i];
                    }
                }
                if (!equipMethod(equipment.getItemType().toUpperCase(Locale.ROOT), gearType.toUpperCase(Locale.ROOT), equipment.getLevelRequirement(), player.getLevel())) {
                    throw new InvalidArmorException("test");
                }
            }


            if (gear.equals("helmet")) {
                if (player.getPlayerClass().equals("mage")){
                    mage.setHelmet(armorname);
                }
                if (player.getPlayerClass().equals("warrior")){
                    mage.setHelmet(armorname);
                }
                if (player.getPlayerClass().equals("rogue")){
                    mage.setHelmet(armorname);
                }
                if (player.getPlayerClass().equals("ranger")){
                    mage.setHelmet(armorname);
                }
                player.setHelmetAttributes(equipment.getStrGain(), equipment.getIntGain(), equipment.getDexGain());
                System.out.println("Intelligence on gear: " + equipment.getIntGain() + "\n" +
                        "Strength on gear: " + equipment.getStrGain() + "\n" +
                        "Dexterity on gear: " + equipment.getDexGain() + "\n" +
                        "Type of item: " + equipment.getItemType()
                );
                System.out.print("Your new item has been made: " + mage.getHelmet() + "\n");
            }

            if (gear.equals("chest")) {
                if (player.getPlayerClass().equals("mage")){
                    mage.setHelmet(armorname);
                }
                if (player.getPlayerClass().equals("warrior")){
                    mage.setHelmet(armorname);
                }
                if (player.getPlayerClass().equals("rogue")){
                    mage.setHelmet(armorname);
                }
                if (player.getPlayerClass().equals("ranger")){
                    mage.setHelmet(armorname);
                }
                player.setChestAttributes(equipment.getStrGain(), equipment.getIntGain(), equipment.getDexGain());
                System.out.println("Intelligence on gear: " + equipment.getIntGain() + "\n" +
                        "Strength on gear: " + equipment.getStrGain() + "\n" +
                        "Dexterity on gear: " + equipment.getDexGain() + "\n" +
                        "Type of item: " + equipment.getItemType()
                );
                System.out.print("Your new item has been made: " + player.getChest() + "\n");
            }

            if (gear.equals("leggings")) {
                if (player.getPlayerClass().equals("mage")){
                    mage.setHelmet(armorname);
                }
                if (player.getPlayerClass().equals("warrior")){
                    mage.setHelmet(armorname);
                }
                if (player.getPlayerClass().equals("rogue")){
                    mage.setHelmet(armorname);
                }
                if (player.getPlayerClass().equals("ranger")){
                    mage.setHelmet(armorname);
                }
                player.setLeggingsAttributes(equipment.getStrGain(), equipment.getIntGain(), equipment.getDexGain());
                System.out.println("Intelligence on gear: " + equipment.getIntGain() + "\n" +
                        "Strength on gear: " + equipment.getStrGain() + "\n" +
                        "Dexterity on gear: " + equipment.getDexGain() + "\n" +
                        "Type of item: " + equipment.getItemType()
                );
                System.out.print("Your new item has been made: " + player.getLeggings() + "\n");
            }
        } catch (InvalidArmorException ignored) {

        }

    }


    public void makeWeapon() {
        String weaponName, weaponType, weaponDamage, attackSpeed, level;
        Scanner maker = new Scanner(System.in);
        boolean weaponChecker = false;
        System.out.println("Enter the name for your weapon: ");
        weaponName = maker.nextLine();

        System.out.println("Enter the weapon type: ");
        weaponType = maker.nextLine();

        System.out.println("Enter the damage of your weapon: ");
        weaponDamage = maker.nextLine();

        System.out.println("Enter the attack speed of your weapon: ");
        attackSpeed = maker.nextLine();

        System.out.println("Enter the attack speed of your weapon: ");
        level = maker.nextLine();
        if (player.getPlayerClass().equals("mage")) {
            for (int i = 0; i < mage.getUsableWeapons().length; i++) {
                if (equipMethod(weaponType.toUpperCase(Locale.ROOT), mage.getUsableWeapons()[i], mage.getLevel(), Integer.parseInt(level))) {
                    weaponChecker = true;
                }

            }
            try {
                if (!weaponChecker) {
                    throw new InvalidWeaponException("You can't equip that!");
                }

                weapon.setName(weaponName);
                weapon.setWeaponType(weaponType);
                weapon.setDamage(Double.parseDouble(weaponDamage));
                weapon.setAttackSpeed(Double.parseDouble(attackSpeed));
                weapon.setLevelRequirement(Integer.parseInt(level));
                mage.setWeapon(weaponName);

            } catch (InvalidWeaponException ex) {
                System.out.println("Please make a valid weapon next time.");
            }
        }
        if (player.getPlayerClass().equals("warrior")) {
            for (int i = 0; i < warrior.getUsableWeapons().length; i++) {
                if (weaponType.toUpperCase().equals(warrior.getUsableWeapons()[i]) && warrior.getLevel() >= Integer.parseInt(level)) {
                    weaponChecker = true;
                }
            }
            try {
                if (!weaponChecker) {
                    throw new InvalidWeaponException("You can't equip that!");
                }

                weapon.setName(weaponName);
                weapon.setWeaponType(weaponType);
                weapon.setDamage(Double.parseDouble(weaponDamage));
                weapon.setAttackSpeed(Double.parseDouble(attackSpeed));
                weapon.setLevelRequirement(Integer.parseInt(level));
                warrior.setWeapon(weaponName);

            } catch (InvalidWeaponException ex) {
                System.out.println("Please make a valid weapon next time.");
            }
        }
        if (player.getPlayerClass().equals("ranger")) {
            for (int i = 0; i < ranger.getUsableWeapons().length; i++) {
                if (weaponType.toUpperCase().equals(ranger.getUsableWeapons()[i]) && ranger.getLevel() >= Integer.parseInt(level)) {
                    weaponChecker = true;
                }
            }
            try {
                if (!weaponChecker) {
                    throw new InvalidWeaponException("You can't equip that!");
                }

                weapon.setName(weaponName);
                weapon.setWeaponType(weaponType);
                weapon.setDamage(Double.parseDouble(weaponDamage));
                weapon.setAttackSpeed(Double.parseDouble(attackSpeed));
                weapon.setLevelRequirement(Integer.parseInt(level));
                ranger.setWeapon(weaponName);

            } catch (InvalidWeaponException ex) {
                System.out.println("Please make a valid weapon next time.");
            }
        }
        if (player.getPlayerClass().equals("rogue")) {
            for (int i = 0; i < rogue.getUsableWeapons().length; i++) {
                if (weaponType.toUpperCase().equals(rogue.getUsableWeapons()[i]) && rogue.getLevel() >= Integer.parseInt(level)) {
                    weaponChecker = true;
                }
            }
            try {
                if (!weaponChecker) {
                    throw new InvalidWeaponException("You can't equip that!");
                }

                weapon.setName(weaponName);
                weapon.setWeaponType(weaponType);
                weapon.setDamage(Double.parseDouble(weaponDamage));
                weapon.setAttackSpeed(Double.parseDouble(attackSpeed));
                weapon.setLevelRequirement(Integer.parseInt(level));
                rogue.setWeapon(weaponName);

            } catch (InvalidWeaponException ex) {
                System.out.println("Please make a valid weapon next time.");
            }
        }
        weaponChecker = false;
    }

    public double calculateDps(double weaponDamage, double attackSpeed, double mainStat) {
        return (weaponDamage * attackSpeed) * (1 + (mainStat / 100));
    }


    public boolean equipMethod(String item, String playableChar, int itemLevel, int playerLevel) {
        return item.equals(playableChar) && playerLevel >= itemLevel;
    }

    public void gearChecker() {
        if (mage.getChest() == null) {
            player.setChestAttributes(0, 0, 0);
        }
        if (mage.getHelmet() == null) {
            player.setHelmetAttributes(0, 0, 0);
        }
        if (mage.getLeggings() == null) {
            player.setLeggingsAttributes(0, 0, 0);
        }


    }

}