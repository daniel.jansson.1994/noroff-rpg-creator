package rpgcharacterpackage;

public class Warrior extends PlayerCharacter {

    private int strAttribute = 5;
    private int dexAttribute = 2;
    private int intAttribute = 1;
    private final String[] usableWeapons = {"AXE", "HAMMER", "SWORD"};
    private final String[] gearType = {"MAIL", "PLATE"};
    private double charDps;

    public Warrior() {
        setStrAttribute();
        setDexAttribute();
        setIntAttribute();
        setBasicAttributes(strAttribute, intAttribute, dexAttribute);
    }

    //GETTERS

    public String[] getGearType() {
        return gearType;
    }

    public String[] getUsableWeapons() {
        return usableWeapons;
    }

    public double getCharDps() {
        return charDps;
    }

    public int getStrAttribute() {
        return strAttribute;
    }

    public int getDexAttribute() {
        return dexAttribute;
    }

    public int getIntAttribute() {
        return intAttribute;
    }

    //SETTERS

    public void setCharDps(double charDps) {
        this.charDps = charDps;
    }

    public void setDexAttribute() {
        this.dexAttribute = 2;
    }

    public void setStrAttribute() {
        this.strAttribute = 5;
    }

    public void setIntAttribute() {
        this.intAttribute = 1;
    }

    //Leveling methods
    public void levelUp() {
        newLevel();
        dexAttribute += levelUpDex();
        intAttribute += levelUpInt();
        strAttribute += levelUpStr();
        setBasicAttributes(strAttribute, intAttribute, dexAttribute);
    }

    public int levelUpDex() {
        return 2;
    }

    public int levelUpStr() {
        return 3;
    }

    public int levelUpInt() {
        return 1;
    }

    //Info about the character
    @Override
    public String toString() {

        return "Name = " + getName() + "\n" +
                "Class = " + getPlayerClass() + "\n" +
                "Level = " + getLevel() + "\n" +
                "Base Intelligence = " + basicAttributes.get("Intelligence") + "\n" +
                "Base Dexterity = " + basicAttributes.get("Dexterity") + "\n" +
                "Base Strength = " + basicAttributes.get("Strength") + "\n" +
                "Total Strength: " + (totalAttributes.get("Strength") + basicAttributes.get("Strength")) + "\n" +
                "Total Dexterity: " + (totalAttributes.get("Dexterity") + basicAttributes.get("Dexterity")) + "\n" +
                "Total Intelligence: " + (totalAttributes.get("Intelligence") + basicAttributes.get("Intelligence")) + "\n" +
                "Dps = " + getCharDps() + "\n";
    }


}
