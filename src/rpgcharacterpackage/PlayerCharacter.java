package rpgcharacterpackage;

import java.util.HashMap;

public abstract class PlayerCharacter {
    private String name;
    private String playerClass;
    private String helmet;
    private String chest;
    private String leggings;
    private String weapon;
    private double charDps;
    private int level = 1;

    public void setCharDps(double charDps) {
        this.charDps = charDps;
    }

    public double getCharDps() {
        return charDps;
    }

    HashMap<String, Integer> chestAttributes = new HashMap<>();
    HashMap<String, Integer> leggingsAttributes = new HashMap<>();
    HashMap<String, Integer> helmetAttributes = new HashMap<>();
    HashMap<String, Integer> basicAttributes = new HashMap<>();
    HashMap<String, Integer> totalAttributes = new HashMap<>();

    //GETTERS
    public String getPlayerClass() {
        return playerClass;
    }

    public int getHelmetStrength() {
        return helmetAttributes.get("Strength");
    }

    public int getHelmetIntelligence() {
        return helmetAttributes.get("Intelligence");
    }

    public int getHelmetDexterity() {
        return helmetAttributes.get("Dexterity");
    }

    public int getChestStrength() {
        return chestAttributes.get("Strength");
    }

    public int getChestIntelligence() {
        return chestAttributes.get("Intelligence");
    }

    public int getChestDexterity() {
        return chestAttributes.get("Dexterity");
    }

    public int getLeggingsStrength() {
        return leggingsAttributes.get("Strength");
    }

    public int getLeggingsIntelligence() {
        return leggingsAttributes.get("Intelligence");
    }

    public int getLeggingsDexterity() {
        return leggingsAttributes.get("Dexterity");
    }

    public String getHelmet() {
        return helmet;
    }

    public int getLevel() {
        return level;
    }

    public String getWeapon() {
        return weapon;
    }

    public String getName() {
        return name;
    }

    public HashMap<String, Integer> getBasicAttributes() {
        return basicAttributes;
    }

    public HashMap<String, Integer> getTotalAttributes() {
        return totalAttributes;
    }

    public String getChest() {
        return chest;
    }

    public String getLeggings() {
        return leggings;
    }

    //SETTERS
    public void setPlayerClass(String playerClass) {
        this.playerClass = playerClass;
    }

    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }

    public void setChest(String chest) {
        this.chest = chest;
    }

    public void setLeggings(String leggings) {
        this.leggings = leggings;
    }

    public void setTotalAttributes(int strength, int intelligence, int dexterity) {
        totalAttributes.put("Strength", strength);
        totalAttributes.put("Intelligence", intelligence);
        totalAttributes.put("Dexterity", dexterity);
    }

    public void setBasicAttributes(int strength, int intelligence, int dexterity) {
        basicAttributes.put("Strength", strength);
        basicAttributes.put("Intelligence", intelligence);
        basicAttributes.put("Dexterity", dexterity);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHelmet(String helmet) {
        this.helmet = helmet;
    }

    public void setHelmetAttributes(int strength, int intelligence, int dexterity) {
        helmetAttributes.put("Strength", strength);
        helmetAttributes.put("Intelligence", intelligence);
        helmetAttributes.put("Dexterity", dexterity);
    }

    public void setChestAttributes(int strength, int intelligence, int dexterity) {
        chestAttributes.put("Strength", strength);
        chestAttributes.put("Intelligence", intelligence);
        chestAttributes.put("Dexterity", dexterity);
    }

    public void setLeggingsAttributes(int strength, int intelligence, int dexterity) {
        leggingsAttributes.put("Strength", strength);
        leggingsAttributes.put("Intelligence", intelligence);
        leggingsAttributes.put("Dexterity", dexterity);
    }

    //Method for leveling
    public int newLevel() {
        return level += 1;
    }

}
