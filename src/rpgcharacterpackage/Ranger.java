package rpgcharacterpackage;

public class Ranger extends PlayerCharacter {


    private final String job = "Ranger";
    private int dexAttribute = 7;
    private int strAttribute = 1;
    private int intAttribute = 1;
    private final String[] gearType = {"LEATHER", "MAIL"};
    private final String[] usableWeapons = {"BOW"};
    private double charDps;

    //Ranger constructor
    public Ranger() {
        setDexAttribute();
        setStrAttribute();
        setIntAttribute();
        setBasicAttributes(strAttribute, intAttribute, dexAttribute);
    }

    //GETTERS
    public String[] getUsableWeapons() {
        return usableWeapons;
    }

    public double getCharDps() {
        return charDps;
    }

    public int getStrAttribute() {
        return strAttribute;
    }

    public String[] getGearType() {
        return gearType;
    }


    public int getDexAttribute() {
        return dexAttribute;
    }

    public int getIntAttribute() {
        return intAttribute;
    }


    //SETTERS
    public void setCharDps(double charDps) {
        this.charDps = charDps;
    }

    public void setDexAttribute() {
        this.dexAttribute = 7;
    }

    public void setStrAttribute() {
        this.strAttribute = 1;
    }

    public void setIntAttribute() {
        this.intAttribute = 1;
    }


    //Levelup methods
    public void levelUp() {
        newLevel();
        dexAttribute += levelUpDex();
        intAttribute += levelUpInt();
        strAttribute += levelUpStr();
        setBasicAttributes(strAttribute, intAttribute, dexAttribute);
    }

    public int levelUpDex() {
        return 5;
    }

    public int levelUpStr() {
        return 1;
    }

    public int levelUpInt() {
        return 1;
    }

    //String to display character stats
    @Override
    public String toString() {

        return "Name = " + getName() + "\n" +
                "Class = " + getPlayerClass() + "\n" +
                "Level = " + getLevel() + "\n" +
                "Base Intelligence = " + basicAttributes.get("Intelligence") + "\n" +
                "Base Dexterity = " + basicAttributes.get("Dexterity") + "\n" +
                "Base Strength = " + basicAttributes.get("Strength") + "\n" +
                "Total Strength: " + (totalAttributes.get("Strength") + basicAttributes.get("Strength")) + "\n" +
                "Total Dexterity: " + (totalAttributes.get("Dexterity") + basicAttributes.get("Dexterity")) + "\n" +
                "Total Intelligence: " + (totalAttributes.get("Intelligence") + basicAttributes.get("Intelligence")) + "\n" +
                "Dps = " + getCharDps() + "\n";
    }
}
