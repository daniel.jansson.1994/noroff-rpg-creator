package rpgcharacterpackage;

public class Mage extends PlayerCharacter {
    private final String job = "Mage";
    private int intAttribute = 8;
    private int dexAttribute = 1;
    private int strAttribute = 1;
    private final String[] usableWeapons = {"WAND", "STAFF"};

    //Constructor

    public Mage() {
        setDexAttribute();
        setIntAttribute();
        setStrAttribute();
        setBasicAttributes(strAttribute, intAttribute, dexAttribute);
    }

    //GETTERS



    public String[] getUsableWeapons() {
        return usableWeapons;
    }

    public String getGearType() {
        return "cloth";
    }

    public int getIntAttribute() {
        return intAttribute;
    }

    public int getDexAttribute() {
        return dexAttribute;
    }

    public int getStrAttribute() {
        return strAttribute;
    }

    //SETTERS



    public void setDexAttribute() {
        this.dexAttribute = 1;
    }

    public void setStrAttribute() {
        this.strAttribute = 1;
    }

    public void setIntAttribute() {
        this.intAttribute = 8;
    }

    //Leveling method

    public void levelUp() {
        newLevel();
        dexAttribute += levelUpDex();
        intAttribute += levelUpInt();
        strAttribute += levelUpStr();
        setBasicAttributes(strAttribute, intAttribute, dexAttribute);
    }

    public int levelUpDex() {
        return 1;
    }

    public int levelUpStr() {
        return 1;
    }

    public int levelUpInt() {
        return 5;
    }

    //String for mage info

    @Override
    public String toString() {

        return "Name = " + getName() + "\n" +
                "Class = " + getPlayerClass() + "\n" +
                "Level = " + getLevel() + "\n" +
                "Base Intelligence = " + basicAttributes.get("Intelligence") + "\n" +
                "Base Dexterity = " + basicAttributes.get("Dexterity") + "\n" +
                "Base Strength = " + basicAttributes.get("Strength") + "\n" +
                "Total Strength: " + (totalAttributes.get("Strength") + basicAttributes.get("Strength")) + "\n" +
                "Total Dexterity: " + (totalAttributes.get("Dexterity") + basicAttributes.get("Dexterity")) + "\n" +
                "Total Intelligence: " + (totalAttributes.get("Intelligence") + basicAttributes.get("Intelligence")) + "\n" +
                "Dps = " + getCharDps() + "\n";
    }
}
