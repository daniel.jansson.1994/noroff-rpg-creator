package rpgcharacterpackage;

public class Rogue extends PlayerCharacter {
    private final String job = "Rogue";
    private int dexAttribute = 6;
    private int strAttribute = 2;
    private int intAttribute = 1;
    private final String[] usableWeapons = {"DAGGER", "SWORD"};
    private final String[] gearType = {"LEATHER", "MAIL"};
    private double charDps;


    //Constructor
    public Rogue() {
        getName();
        setDexAttribute();
        setStrAttribute();
        setIntAttribute();
        setBasicAttributes(strAttribute, intAttribute, dexAttribute);
    }

    //GETTERS

    public double getCharDps() {
        return charDps;
    }

    public int getStrAttribute() {
        return strAttribute;
    }

    public int getDexAttribute() {
        return dexAttribute;
    }

    public int getIntAttribute() {
        return intAttribute;
    }

    public String[] getGearType() {
        return gearType;
    }

    public String[] getUsableWeapons() {
        return usableWeapons;
    }


    //SETTERS
    public void setCharDps(double charDps) {
        this.charDps = charDps;
    }

    public void setDexAttribute() {
        this.dexAttribute = 6;
    }

    public void setStrAttribute() {
        this.strAttribute = 2;
    }

    public void setIntAttribute() {
        this.intAttribute = 1;
    }


    //Leveling methods
    public void levelUp() {
        newLevel();
        dexAttribute += levelUpDex();
        intAttribute += levelUpInt();
        strAttribute += levelUpStr();
        setBasicAttributes(strAttribute, intAttribute, dexAttribute);
    }

    public int levelUpDex() {
        return 4;
    }

    public int levelUpStr() {
        return 1;
    }

    public int levelUpInt() {
        return 1;
    }


    //Method to give the character info
    @Override
    public String toString() {

        return "Name = " + getName() + "\n" +
                "Class = " + getPlayerClass() + "\n" +
                "Level = " + getLevel() + "\n" +
                "Base Intelligence = " + basicAttributes.get("Intelligence") + "\n" +
                "Base Dexterity = " + basicAttributes.get("Dexterity") + "\n" +
                "Base Strength = " + basicAttributes.get("Strength") + "\n" +
                "Total Strength: " + (totalAttributes.get("Strength") + basicAttributes.get("Strength")) + "\n" +
                "Total Dexterity: " + (totalAttributes.get("Dexterity") + basicAttributes.get("Dexterity")) + "\n" +
                "Total Intelligence: " + (totalAttributes.get("Intelligence") + basicAttributes.get("Intelligence")) + "\n" +
                "Dps = " + getCharDps() + "\n";
    }
}
