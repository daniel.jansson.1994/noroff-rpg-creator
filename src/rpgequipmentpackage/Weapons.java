package rpgequipmentpackage;

public class Weapons {
    private String weaponType;
    private double damage;
    private double attackSpeed;
    private int levelRequirement;

    //GETTERS

    public int getLevelRequirement() {
        return levelRequirement;
    }

    public String getWeaponType() {
        return weaponType;
    }

    public double getDamage() {
        return damage;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    //SETTERS

    public void setName(String name) {
    }

    public void setWeaponType(String weaponType) {
        this.weaponType = weaponType;
    }

    public void setDamage(double damage) {
        this.damage = damage;
    }

    public void setAttackSpeed(double attackSpeed) {
        this.attackSpeed = attackSpeed;
    }

    public void setLevelRequirement(int levelRequirement) {
        this.levelRequirement = levelRequirement;
    }
}
