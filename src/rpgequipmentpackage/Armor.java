package rpgequipmentpackage;

public class Armor {

    private String itemName;
    private String itemType;
    private String itemSlot;
    private int levelRequirement;
    private int strGain;
    private int dexGain;
    private int intGain;

    //Constructor

    public Armor(String itemName, String itemType, String itemSlot, int levelRequirement, int strGain, int dexGain, int intGain) {
        setItemName(itemName);
        setItemType(itemType);
        setItemSlot(itemSlot);
        setLevelRequirement(levelRequirement);
        setStrGain(strGain);
        setDexGain(dexGain);
        setIntGain(intGain);
    }

    //GETTERS

    public int getLevelRequirement() {
        return levelRequirement;
    }

    public int getStrGain() {
        return strGain;
    }

    public int getDexGain() {
        return dexGain;
    }

    public int getIntGain() {
        return intGain;
    }

    public String getItemType() {
        return itemType;
    }

    //SETTERS

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public void setItemSlot(String itemSlot) {
        this.itemSlot = itemSlot;
    }

    public void setLevelRequirement(int levelRequirement) {
        this.levelRequirement = levelRequirement;
    }

    public void setStrGain(int strGain) {
        this.strGain = strGain;
    }

    public void setDexGain(int dexGain) {
        this.dexGain = dexGain;
    }

    public void setIntGain(int intGain) {
        this.intGain = intGain;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    //String with info about the item

    @Override
    public String toString() {
        return "Armor{" +
                "itemName='" + itemName + '\'' +
                ", itemType='" + itemType + '\'' +
                ", itemSlot='" + itemSlot + '\'' +
                ", levelRequirement=" + levelRequirement +
                ", strGain=" + strGain +
                ", dexGain=" + dexGain +
                ", intGain=" + intGain +
                '}';
    }

}
