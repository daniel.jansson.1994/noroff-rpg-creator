import rpggame.Game;
import java.util.Scanner;

public class Main {

    public  static void main(String[] args) {
        Scanner option = new Scanner (System.in);
        boolean playing = true;
        Game game = new Game();
        game.characterSelector();

        do{
            String answer;

            System.out.println("What do you want to do?");
            System.out.println("Options: level, gear, stats, exit. ");
            answer = option.nextLine().toLowerCase();

            switch (answer) {
                case "level" -> game.levelup();
                case "gear" -> game.selectGear();
                case "stats" -> game.stats();
                case "exit" -> playing = false;
                default -> System.out.print("Please select an existing value!\n");
            }
        } while (playing);

    }
}
